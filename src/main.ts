import { createApp } from 'vue'
import App from './App.vue'
import router from './router';
import VueLazyLoad from 'vue3-lazyload';
import './style/responsives.css';
import './style/variables.css';
import './style/classes.css';
import './style/animations.css';

const app = createApp(App);

app.use(VueLazyLoad)
app.use(router).mount('#app')
