import { reactive, computed } from 'vue';
import Videos from '../data/videos';

const inp = {
    id: '',
    name: '',
    director: '',
    views: '',
    img: ''
}

const inPlayer = reactive(inp)

const getInplayer = computed(() => inPlayer);

function ResetVid(): void {
    getInplayer.value.id = '';
    getInplayer.value.name = '';
    getInplayer.value.director = '';
    getInplayer.value.views = '';
    getInplayer.value.img = '';
}

async function getData(id: string): Promise<boolean | undefined> {
    try {
        
        const vid = Videos.find((e) => e.id === id);
        if (!vid) {
            throw new Error('Les datas n\ont été récupérées');
        }
        getInplayer.value.id = vid?.id as string;
        getInplayer.value.name = vid?.name as string;
        getInplayer.value.director = vid?.director as string;
        getInplayer.value.views = vid?.views as string;
        getInplayer.value.img = vid?.img as string;
        return true;
    } catch (err: any) {
        console.log(err)
    }

}

export default {
    getInplayer,
    getData,
    ResetVid
}
