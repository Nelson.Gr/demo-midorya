import { reactive, computed } from 'vue';

const statePopup = reactive({
    popup: false,
    message: '',
    clr: ''
})
const modalPopup = reactive({
    popup: false,
    message: '',
    clr: ''
})

const getPopup = computed(() => statePopup);
const getPopup2 = computed(() => modalPopup);

function setPopup(msg:string, clr: string): void {
    getPopup.value.popup = true;
    getPopup.value.message = msg;
    getPopup.value.clr = clr;
    setTimeout(() => {
        getPopup.value.popup = false;
        getPopup.value.message = '';
        getPopup.value.clr = '';
    }, 4000)
}

function setModalPopup(msg:string, clr: string): void {
    getPopup2.value.popup = true;
    getPopup2.value.message = msg;
    getPopup2.value.clr = clr;
    setTimeout(() => {
        getPopup2.value.popup = false;
        getPopup2.value.message = '';
        getPopup2.value.clr = '';
    }, 4000)
}

const popup = {
    getPopup, getPopup2, setModalPopup, setPopup
}

export default popup;