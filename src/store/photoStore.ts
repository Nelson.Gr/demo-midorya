import { reactive, computed } from "vue";
import { Photos } from '../data/photos';
import { IPhoto } from '../interfaces';

let listType: any;
const stPhoto = reactive({
    list: listType as IPhoto[],
    isLoading: true
})



const getPhotos = computed(() => stPhoto);

const clAll = async (): Promise<IPhoto[]> => {
    return Photos;
    
}

const clPor = async (): Promise<IPhoto[]> => {
    return Photos.filter((e) => e.layout === 0);
}
const clPay = async (): Promise<IPhoto[]> => {
    return Photos.filter((e) => e.layout === 1);
}

export default {
    getPhotos, clAll, clPay, clPor
}