import { reactive, computed } from "vue";
import francais from '../languages/fr';
import english from '../languages/en';

const lang = reactive({
    opt: 'fr'
});

const getLang = computed(() => lang);
const isLang = computed(() => {
    if (getLang.value.opt === 'en') {
        return english;
    } else {
        return francais;
    }
});

function setLang(l: string): void {
    getLang.value.opt = l;
    localStorage.setItem('midoLang', l);
}

export default {
    getLang, isLang, setLang
}