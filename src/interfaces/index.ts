 export interface IPhoto {
    id: number;
    director: string;
    layout: number,
    img: string;
}