import { IPhoto } from '../interfaces';
export const Photos: IPhoto[] = [
    {
        id: 1,
        director: 'Mikko Saito',
        layout: 0,
        img: 'https://res.cloudinary.com/deccpjpzw/image/upload/v1655561320/midorya/mido/por4_ffcfnt_fmrl4j.webp',
    },
    {
        id: 2,
        director: 'Nolan Jones',
        layout: 1,
        img: 'https://res.cloudinary.com/deccpjpzw/image/upload/v1655561270/midorya/mido/pay10_xq3drk_mzrbou.webp',
    },
    {
        id: 3,
        director: 'Markus Stundberg',
        layout: 1,
        img: 'https://res.cloudinary.com/deccpjpzw/image/upload/v1655561263/midorya/mido/pay9_ybvi2n_zjofck.webp',
    },
    {
        id: 4,
        director: 'Marlo Corvano',
        layout: 1,
        img: 'https://res.cloudinary.com/deccpjpzw/image/upload/v1655561256/midorya/mido/pay8_yzlxj7_s7h57o.webp',
    },
    {
        id: 5,
        director: 'Rick Dills',
        layout: 0,
        img: 'https://res.cloudinary.com/deccpjpzw/image/upload/v1655561400/midorya/mido/por10_ifi8yj_t7btbf.webp',
    },
    {
        id: 6,
        director: 'Meryl Smith',
        layout: 1,
        img: 'https://res.cloudinary.com/deccpjpzw/image/upload/v1655561242/midorya/mido/pay7_evtw93_sekvkh.webp',
    },
    {
        id: 7,
        director: 'Damien Viroux',
        layout: 1,
        img: 'https://res.cloudinary.com/deccpjpzw/image/upload/v1655561228/midorya/mido/pay6_t5b1im_mcwxon.webp',
    },
    {
        id: 8,
        director: 'Markus Stundberg',
        layout: 1,
        img: 'https://res.cloudinary.com/deccpjpzw/image/upload/v1655561220/midorya/mido/pay5_qthjpr_jyefd1.webp',
    },
    {
        id: 9,
        director: 'Rick Dills',
        layout: 1,
        img: 'https://res.cloudinary.com/deccpjpzw/image/upload/v1655561212/midorya/mido/pay4_aot1pg_ul0cvw.webp',
    },
    {
        id: 10,
        director: 'Valery Porter',
        layout: 0,
        img: 'https://res.cloudinary.com/deccpjpzw/image/upload/v1655561290/midorya/mido/por1_m582p2_rlilci.webp',
    },
    {
        id: 11,
        director: 'Ted Kolan',
        layout: 1,
        img: 'https://res.cloudinary.com/deccpjpzw/image/upload/v1655561198/midorya/mido/pay3_scthzi_vj08nh.webp',
    },
    {
        id: 12,
        director: 'Ryo Wantanabe',
        layout: 0,
        img: 'https://res.cloudinary.com/deccpjpzw/image/upload/v1655561342/midorya/mido/por6_ahnmj5_pe2svy.webp',
    },
    {
        id: 13,
        director: 'Madeline Fréchet',
        layout: 1,
        img: 'https://res.cloudinary.com/deccpjpzw/image/upload/v1655561190/midorya/mido/pay1_ez5ful_g2kzk2.webp',
    },
    {
        id: 14,
        director: 'Piotr Nowak',
        layout: 0,
        img: 'https://res.cloudinary.com/deccpjpzw/image/upload/v1655561392/midorya/mido/por9_bhf4sg_v7feb8.webp',
    },

    {
        id: 15,
        director: 'Simen Dahl',
        layout: 1,
        img: 'https://res.cloudinary.com/deccpjpzw/image/upload/v1655561181/midorya/mido/pat2_ykhn8y_nokonm.webp',
    },
    {
        id: 16,
        director: 'Gwen Shanahan',
        layout: 0,
        img: 'https://res.cloudinary.com/deccpjpzw/image/upload/v1655561333/midorya/mido/por5_qwnlsb_zugwmm.webp',
    },
    {
        id: 17,
        director: 'Diogo Griezmann',
        layout: 0,
        img: 'https://res.cloudinary.com/deccpjpzw/image/upload/v1655561300/midorya/mido/por2_few1dk_xpodbh.webp',
    },
]
