import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import Home from '../views/Home.vue'

const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        meta: {transition: 'fade'}
    },
    {
        path: '/nos-videos/:id',
        name: 'VideosPage',
        component: () => import('../views/VideosPage.vue'),
        meta: {transition: 'fade'}
    },
    {
        path: '/nos-photos/:layout',
        name: 'PhotosPage',
        component: () => import('../views/PhotosPage.vue'),
        meta: {transition: 'fade'}
    },
    {
        path: '/contact/:director',
        name: 'ContactPage',
        component: () => import('../views/ContactPage.vue')
    },
    {
        path: '/:pathMatch(.*)*',
        name: 'NotFound',
        component: () => import('../views/NotFound.vue')
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes
  })

  export default router;