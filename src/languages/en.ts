export default {
    home: {
        head: {
            cont1: 'Our Videos',
            cont2: 'Our Photos',
            lang: 'Languages'
        },
        banner1: {
            p1: 'Telling',
            p2: 'the life',
            p3: 'with images'
        },
        banner2: {
            title: 'About us',
            txt: `
            Midorya is a collective of creatives passionate about imaging.
             Whether to immortalize your moments of life or for original creations, 
             we will be with you for any kind of project.`,
            des1: 'Perry Green is shooting on parisian roofs.',
            des2: 'Ted et Fred Kolan is cliping a brand new OG Loc\'s track.',
            des3: 'Gwen Shanahan to our newest studio to editing a projet.',
            des4: 'Emmanuel Lina is directing a short film: "Distance".',
            des5: 'Sean Taylor is shooting to promote a luxury pinthouse.'
        },
        banner3: {
            title: 'The founders',
            post1: 'Web Developer',
            post2: 'Photographer',
            post3: 'Video editor',
            post4: 'Video maker'
        },
        foot: {
            l1: 'Cookie preferences',
            l2: 'Legal notices'
        }
    },
    photos: {
        head: 'Gallery',
        title: 'Everywhere in the world',
        contact: 'Contact',
        prefix: 'By',
        footer: '"Immortalized with love and creativity"',
        layout: {
            lay1: 'All',
            lay2: 'Landscape',
            lay3: 'Portrait'
        }
    },
    videos: {
        head: 'Playlist',
        side: {
            title: 'Others videos',
            vues: 'views',
            end: 'Nothing more'
        },
        main: {
            btn1: 'Share',
            btn2: 'Contact'
        }
    },
    contact: {
        head: 'Contact',
        btn: 'Send',
        area: 'New message',
        obj: 'Object',
        to: 'To'
    },
    popup: {
        msg1: 'This link doesn\'t work in the demo',
        msg2: 'Thank for having fun',
        msg3: 'Downloading coming soon !',
        warn1: 'Type your email address, please',
        warn2: 'Are you sure you typed an email address?',
        warn3: 'Type an object for to introduce your message, please',
        warn4: 'Complete every inputs before sending your message',
        warn5: 'It\'s not valid or too short',
        warn6: 'The text area is completely empty'
    }
}