export default {
    home: {
        head: {
            cont1: 'Nos Videos',
            cont2: 'Nos Photos',
            lang: 'Langues'
        },
        banner1: {
            p1: 'le top',
            p2: 'des créateurs',
            p3: 'photo et video'
        },
        banner2: {
            title: 'Qui sommes nous ?',
            txt: `Midorya est un collectif de créatifs passionnés par la mise en image.
            Que ce soit pour immortaliser vos instants de vie ou pour des créations originales,
             nous vous accompagnons pour toute sorte de projet.`,
            des1: 'Perry Green lors d\'un séance photo en sur les toits de Paris.',
            des2: 'Ted et Fred Kolan sur le tounage du clip "Groove street" du rappeur OG Loc.',
            des3: 'Gwen Shanahan en plein travail de post-production.',
            des4: 'Emmanuel Lina sur le tounage du court-métrage "La distance".',
            des5: 'Sean Taylor en plein shooting pour la video promotionelle d\'un appartement.'
        },
        banner3: {
            title: 'Les fondateurs',
            post1: 'Développeur web',
            post2: 'Photographe',
            post3: 'Monteuse video',
            post4: 'Vidéastre'
        },
        foot: {
            l1: 'Préférences cookies',
            l2: 'Mentions légales'
        }
    },
    photos: {
        head: 'Gallerie',
        title: 'Aux quatres coins du monde',
        contact: 'Contacter',
        prefix: 'Par',
        footer: '"Immortalisés avec un grain de folie"',
        layout: {
            lay1: 'Tous',
            lay2: 'Paysage',
            lay3: 'Portrait'
        }
    },
    videos: {
        head: 'Bibliotèque',
        side: {
            title: 'Autres vidéos',
            vues: 'vues',
            end: 'Fin de liste'
        },
        main: {
            btn1: 'Partager',
            btn2: 'Contacter'
        }
    },
    contact: {
        head: 'Contacter',
        btn: 'Envoyer',
        area: 'Nouveau message',
        obj: 'Objet',
        to: 'À'
    },
    popup: {
        msg1: 'Ce lien n\'est pas fontionnel dans cette démo',
        msg2: 'Merci d\'avoir joué le jeu jusqu\'au bout',
        msg3: 'La fonction de téléchargement arrivera prochainement!',
        warn1: 'Indiquer votre adresse mail pour passer à la suite',
        warn2: 'Etes-vous sur d\'avoir saisie une adresse correcte?',
        warn3: 'Veuillez ajouter l\'objet de votre message',
        warn4: 'Veuillez remplir les champs requis',
        warn5: 'Saisie invalide ou trop courte',
        warn6: 'Le champ message est vide'
    }
}