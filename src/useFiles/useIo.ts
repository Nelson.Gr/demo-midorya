import { ref, Ref } from 'vue';

export default function useIo() {
    const target0 = ref();
    const target1 = ref();
    const anime0 = ref(false);
    const anime1 = ref(false);
    
    function lookAndMove(bln: Ref<boolean>, elref: Ref<any>, tld: number) {
        const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
           bln.value = true;
        } else {
           bln.value = false;
        }
      })
    }, {
      threshold: tld
    });
      observer.observe(elref.value)
    }
      return {
          lookAndMove,
          target0, target1, anime0, anime1
      }
}
